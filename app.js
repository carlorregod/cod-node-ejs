//Siempre va acá estas 3 líneas
const express = require("express");
const bodyParser = require('body-parser')
const path = require('path');
const app = express();


require('dotenv').config()


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

const port = process.env.PORT || 3000;
//Conexion BD Mongo
const mongoose = require('mongoose');
const usuario = process.env.DB_USER
const password = process.env.DB_PASWORD
const dbName = process.env.DB_NAME
const uri= `mongodb://${usuario}:${password}@cluster0-shard-00-00.mso3n.mongodb.net:27017,cluster0-shard-00-01.mso3n.mongodb.net:27017,cluster0-shard-00-02.mso3n.mongodb.net:27017/${dbName}?ssl=true&replicaSet=atlas-7a0hyg-shard-0&authSource=admin&retryWrites=true&w=majority`

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(()=>console.log('BD conectada'))
  .catch((err)=>console.log('error de conexion',err))
//Fin conexión a mongodb

//Usando ejs, engine de plantilla
app.set('view engine', 'ejs');
app.set("views", __dirname + "/views");
//Fin configuración motor de plantilla
var publicPath = path.resolve(__dirname, 'public');
//Middlewares para static para poder usar el public
app.use(express.static(publicPath));
//Middleware de rutas web, las rutas serán definidas en router/web.js
app.use('/',require('./router/web'),express.static(publicPath)); // express.static('public') sirve para los asset dentro de ejs
app.use('/mascotas',require('./router/Mascotas'),express.static(publicPath));


//Middleware de routers

//Routers, contenido antiguo, queda comentado
/* app.get("/", (req, res) => {
  res.render("index", {
    titulo:'titulo dinamico'
  });
});
}); */

//Estatico, se deja comentado solo a modo de curiosidad
/* app.get("/services",(req,res)=>{
    res.send('Soy un servicio')
}) */

//Middleware que muestra página 404 ante errores, al final de cada ruta
app.use((req,res,next)=>{
  // res.status(404).sendFile(__dirname + "/public/404.html") //Sólo válido desde página estática, actualmente noe xistwe
  res.status(404).render("templates/404")
});


//Escuchar petición, siempre va al final
app.listen(port, () => {
  console.log(`App escucha el puerto: ${port}`);
});