const express = require("express")
const router = express.Router()

//LLamando al modelo
const Mascota = require('../models/Mascota')

//Datos en duro de prueba
arrayTest = [
    {'id':'jkfjkdfjkdf','nombre':'Rex','descripcion':'rex descripcion'},
    {'id':'adsds','nombre':'Luna','descripcion':'luna descripcion'},
    {'id':'weerr','nombre':'Mia','descripcion':'miaLuna  descripcion'},
]


router.get('/', async(req, res) => {

    //Como query desde mongoDB
    const arrayMascotas = await Mascota.find()
    res.render("mascotas/mascotas", {
        //arrayMascotas: arrayTest //para probar data estática
        arrayMascotas //desde MongoDB
    })
})
//Crear
router.get('/crear', (req, res) => {
    res.render('mascotas/crear')
})
router.post('/crear', async(req, res) => {
    console.log(req.body)
    try {
        //Forma 1
        const mascotaDB = new Mascota(req.body)
        await mascotaDB.save()
        //forma 2
        //Mascota.create(req.body)
        res.redirect('/mascotas')
    } catch (error) {
        console.log('error', error)
    }
})
//Ver
router.post('/see', async(req, res) => {
    const id = req.body.id
    console.log('*****')
    console.log(id)
    try {
        const mascotaDB = await Mascota.findOne({ _id: id }) //encuentra la mascota de ese id determinado
        console.log(mascotaDB)
        return res.json({
            mascota: mascotaDB,
            error: false,
            mensaje: 'Datos de la mascota'
        })
    } catch (error) {
        console.log('erroooooooooorrr', error)
        return res.json({
            mascota:{},
            error: true,
            mensaje: 'No se encuentra el documento...'
        })
    }
})

//Eliminar
router.delete('/:id', async (req, res) => {
    const id = req.params.id;
    console.log('id desde backend', id)
    try {

        const mascotaDB = await Mascota.findByIdAndDelete({ _id: id });
        console.log(mascotaDB)

        if (!mascotaDB) {
            res.json({
                estado: false,
                mensaje: 'No se puede eliminar'
            })
        } else {
            res.json({
                estado: true,
                mensaje: 'eliminado!'
            })
        }
        
    } catch (error) {
        console.log(error)
    }
})

//Actualizar

router.get('/editar?:id', async(req, res) => {
    const id = req.query.id
    console.log(id);
    try{
        const mascotaDB = await Mascota.findOne({ _id: id })
        console.log(mascotaDB)
        res.render('mascotas/editar', {
            mascota: mascotaDB,
            error: false
        })
    } catch (error) {
        console.log('erroooooooooorrr', error)
        res.render('mascotas/editar', {
            error: true,
            mensaje: 'No se encuentra el documento...'
        })
    }
})

router.put('/:id', async (req, res) => {
    const id = req.params.id;
    const body = req.body;

    console.log(id)
    console.log('body', body)

    try {
        const mascotaDB = await Mascota.findByIdAndUpdate(
            id, body, { useFindAndModify: false }
        )
        console.log(mascotaDB)
        res.json({
            estado: true,
            mensaje: 'Mascota editada'
        })
    } catch (error) {
        console.log(error)
        res.json({
            estado: false,
            mensaje: 'Mascota falla'
        })
    }
})

module.exports = router;