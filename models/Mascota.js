const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//La definición del esquema viene dada siempre a la estructura del componente a referir, en este ejemplo, mascota
const mascotaSchema = new Schema({
  nombre:  String,
  descripcion: String
});

// Crear el modelo
const Mascota = mongoose.model('Mascota', mascotaSchema);

module.exports = Mascota;